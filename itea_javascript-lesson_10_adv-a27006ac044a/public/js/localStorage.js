/*

  localStorage
  window.localStorage

*/

// Запись в ЛС
// localStorage.setItem('myCat', 'Tom');
// localStorage.setItem('back', 'green');
// // Чтение с ЛС
// var cat = localStorage.getItem("myCat");
// // Удаление с ЛС
// // localStorage.removeItem("myCat");

// // Если не найдено, вернет Null
// var background = localStorage.getItem("back");
// console.log(background);

// if( background !== null){
//   document.body.style.backgroundColor = background;
// }
// var button1 = document.getElementById('button1');
// console.log(button1);
// button1.addEventListener('click', function () {
//   function getRandomIntInclusive(min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return (Math.floor(Math.random() * (max - min + 1)) + min);       
//   }
//   var a = getRandomIntInclusive(1,255);
//   var b = getRandomIntInclusive(1,255);
//   var c = getRandomIntInclusive(1,255);
//   console.log(a, b, c);
//   var color = 'rgb(' + a + ',' + b + ',' + c + ')';
//   document.body.style.background = color;
//   localStorage.setItem('back', color); 
// });



//   var background = localStorage.getItem('back');
//   if (background !== null) {
//     document.body.style.background = background;
//   }

  var button2 = document.getElementById('inp_submit');
  button2.addEventListener('click', mmm);
  function mmm() {
    var a = document.getElementById('inp_login').value;
    var b = document.getElementById('inp_password').value;
    console.log(a + b);
    localStorage.setItem('input1', a);
    localStorage.setItem('input2', b);
    var count = 0;
    if ((a === 'admin@example.com') && (b === '12345678')) {
      count = 1;
    }
    else {
      count = 0;
    }
    return count;
  }
  var fir_input = localStorage.getItem('input1');
  var sec_input = localStorage.getItem('input2');
  var c = mmm();
  if (c == 1) {
    if ((fir_input !== null) && (sec_input !== null)) {
   
      inp_login.setAttribute('disabled', 'disabled');
      inp_password.setAttribute('disabled', 'disabled');
      inp_submit.setAttribute('disabled', 'disabled');
      document.body.innerText = 'Привет {username}';

    }
  }
  else {
    document.body.innerText = 'Sorry, but your password or login is incorect. Try again.';
  }
  

/*

  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/
